package carpdriver

type PluginService struct {
	Plugin Plugin
}

func (s *PluginService) Configure(configuration []byte) ([]byte, error) {
	return nil, s.Plugin.Configure(configuration)
}

func (c *PluginService) RequestStart(payload []byte) ([]byte, error) {
	r, err := ReqFromBytes(payload)
	if err != nil {
		return nil, err
	}

	req, res, err := c.Plugin.RequestStart(r)
	if err != nil {
		return nil, err
	}

	if res != nil {
		resBytes, err := res.ToBytes()
		if err != nil {
			return nil, err
		}

		return resBytes, nil
	}

	if req != nil {
		reqBytes, err := req.ToBytes()
		if err != nil {
			return nil, err
		}

		return reqBytes, nil
	}

	return nil, nil
}

func (c *PluginService) RequestEnd(payload []byte) ([]byte, error) {
	r, err := ResFromBytes(payload)
	if err != nil {
		return nil, err
	}

	res, err := c.Plugin.RequestEnd(r)
	if err != nil {
		return nil, err
	}

	if res != nil {
		resBytes, err := res.ToBytes()
		if err != nil {
			return nil, err
		}

		return resBytes, nil
	}

	return nil, nil
}
