package carpdriver

import (
	"strings"

	fbs "github.com/google/flatbuffers/go"
	"github.com/google/uuid"
	"gitlab.com/carp-suite/plugins/carpdriver/flatbuffers"
	pb "gitlab.com/carp-suite/plugins/carpdriver/proto"
)

type Response struct {
	ID      uuid.UUID
	Path    string
	Headers map[string]string
	Status  int
	Cookies []*Cookie
	Body    []byte
}

func ResFromBytes(payload []byte) (*Response, error) {
	response := flatbuffers.GetRootAsResponse(payload, 0)

	id, err := uuid.FromBytes(response.IdBytes())
	if err != nil {
		return nil, err
	}

	res := &Response{
		ID:      id,
		Path:    string(response.Path()),
		Headers: make(map[string]string),
		Cookies: make([]*Cookie, 0),
		Status:  int(response.Status()),
		Body:    response.BodyBytes(),
	}

	h := new(flatbuffers.Header)
	for i := 0; i < response.HeadersLength()/2; i++ {

		exists := response.Headers(h, i)
		if !exists {
			continue
		}

		headerKey := string(h.Key())
		headerValue := string(h.Value())

		if strings.ToLower(headerKey) == "set-cookie" {
			res.Cookies = CookiesStringToArray(headerValue)
			continue
		}

		res.Headers[headerKey] = headerValue
	}

	return res, nil
}

func (res *Response) ToBytes() ([]byte, error) {
	resBuilder := new(fbs.Builder)
	id := resBuilder.CreateByteVector(res.ID[:])
	path := resBuilder.CreateString(res.Path)
	body := resBuilder.CreateByteVector(res.Body)
	headers := make([]fbs.UOffsetT, len(res.Headers))

	for k, v := range res.Headers {
		key := resBuilder.CreateByteString([]byte(k))
		value := resBuilder.CreateByteString([]byte(v))
		flatbuffers.HeaderStart(resBuilder)
		flatbuffers.HeaderAddKey(resBuilder, key)
		flatbuffers.HeaderAddValue(resBuilder, value)
		headers = append(headers, flatbuffers.HeaderEnd(resBuilder))
	}

	if len(res.Cookies) != 0 {
		cookies := CookiesToHeaderString(res.Cookies)
		key := resBuilder.CreateByteString([]byte("Set-Cookie"))
		value := resBuilder.CreateByteString([]byte(cookies))
		flatbuffers.HeaderStart(resBuilder)
		flatbuffers.HeaderAddKey(resBuilder, key)
		flatbuffers.HeaderAddValue(resBuilder, value)
		headers = append(headers, flatbuffers.HeaderEnd(resBuilder))
	}

	flatbuffers.ResponseStartHeadersVector(resBuilder, len(headers))
	for _, header := range headers {
		resBuilder.PrependUOffsetT(header)
	}
	finalHeaders := resBuilder.EndVector(len(headers))

	flatbuffers.ResponseStart(resBuilder)
	flatbuffers.ResponseAddId(resBuilder, id)
	flatbuffers.ResponseAddPath(resBuilder, path)
	flatbuffers.ResponseAddStatus(resBuilder, int16(res.Status))
	flatbuffers.ResponseAddBody(resBuilder, body)
	flatbuffers.ResponseAddHeaders(resBuilder, finalHeaders)
	resRoot := flatbuffers.ResponseEnd(resBuilder)
	resBuilder.Finish(resRoot)
	return resBuilder.FinishedBytes(), nil
}

func ResFromProto(res *pb.HttpResponse) (*Response, error) {
	id, err := uuid.FromBytes(res.Id)
	if err != nil {
		return nil, err
	}

	response := &Response{
		ID:      id,
		Path:    res.Path,
		Headers: make(map[string]string),
		Status:  int(res.Status),
		Body:    res.Body,
	}

	for key, value := range res.Headers {
		if strings.ToLower(key) == "set-cookie" {
			response.Cookies = CookiesStringToArray(value)
			continue
		}
		response.Headers[key] = value
	}

	return response, nil
}

func (res *Response) ToProto() *pb.HttpResponse {
	result := &pb.HttpResponse{
		Id:      res.ID[:],
		Headers: res.Headers,
		Status:  int32(res.Status),
		Body:    res.Body,
	}

	if len(res.Cookies) != 0 {
		result.Headers["Set-Cookie"] = CookiesToHeaderString(res.Cookies)
	}

	return result
}
