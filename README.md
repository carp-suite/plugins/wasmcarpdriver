# CARP Driver

Lib by CARP plugins

## 🐞 Testing

In the root of the project exec:

```bash
go test ./.../
```

## Build

Protobuff:

```bash
protoc --go_out=./proto --go_opt=paths=source_relative --go-grpc_out=./proto --go-grpc_opt=paths=source_relative ./carp.proto
```
