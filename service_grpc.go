package carpdriver

import (
	"context"
	"io"
	"log"
	"sync"

	"github.com/google/uuid"
	pb "gitlab.com/carp-suite/plugins/carpdriver/proto"
)

type PluginServiceGrpc struct {
	pb.UnimplementedPluginServer
	Plugin Plugin
	wgRs   sync.WaitGroup
	wgRe   sync.WaitGroup
	muS    sync.Mutex
	muE    sync.Mutex
}

func (s *PluginServiceGrpc) Configure(ctx context.Context, configuration *pb.ConfigurationRequest) (*pb.ConfigurationResponse, error) {
	err := s.Plugin.Configure([]byte(configuration.JsonString))
	if err != nil {
		return nil, err
	}

	return &pb.ConfigurationResponse{}, nil
}

func (s *PluginServiceGrpc) RequestStart(r pb.Plugin_RequestStartServer) error {
	for {
		reqProto, err := r.Recv()

		if err == io.EOF {
			s.wgRs.Wait()
			return nil
		}

		if err != nil {
			return err
		}

		err = s.requestStart(reqProto, r)
		if err != nil {
			return err
		}
	}
}

func (s *PluginServiceGrpc) requestStart(reqProto *pb.HttpRequest, r pb.Plugin_RequestStartServer) error {
	s.wgRs.Add(1)
	defer s.wgRs.Done()
	var result *pb.RequestStartResult
	request, err := ReqFromProto(reqProto)
	if err != nil {
		return err
	}

	req, res, err := s.execRequestStart(request)
	if err != nil {
		return err
	}

	if res != nil {
		result = &pb.RequestStartResult{
			Result: &pb.RequestStartResult_Response{
				Response: res.ToProto(),
			},
		}

		log.Println(result)
		r.Send(result)
		return nil
	}

	if req != nil {
		result = &pb.RequestStartResult{
			Result: &pb.RequestStartResult_Request{
				Request: req.ToProto(),
			},
		}

		log.Println(result)
		r.Send(result)
		return nil
	}

	result = &pb.RequestStartResult{
		Result: &pb.RequestStartResult_Void{
			Void: &pb.HttpVoidResponse{
				Id: request.ID[:],
			},
		},
	}
	log.Println("id: ", request.ID)
	log.Println(result)
	r.Send(result)
	return nil
}

func (s *PluginServiceGrpc) execRequestStart(request *Request) (*Request, *Response, error) {
	s.muS.Lock()
	defer s.muS.Unlock()
	return s.Plugin.RequestStart(request)
}

func (s *PluginServiceGrpc) RequestEnd(r pb.Plugin_RequestEndServer) error {
	for {
		resProto, err := r.Recv()

		if err == io.EOF {
			s.wgRe.Wait()
			return nil
		}

		if err != nil {
			return err
		}

		s.wgRe.Add(1)
		s.requestEnd(resProto, r)
	}
}

func (s *PluginServiceGrpc) requestEnd(resProto *pb.HttpResponse, r pb.Plugin_RequestEndServer) error {
	defer s.wgRe.Done()
	var result *pb.RequestEndResult
	response, err := ResFromProto(resProto)
	if err != nil {
		return err
	}

	res, err := s.execRequestEnd(response)
	if err != nil {
		return err
	}

	if res != nil {
		result = &pb.RequestEndResult{
			Result: &pb.RequestEndResult_Response{
				Response: res.ToProto(),
			},
		}

		r.Send(result)
	}

	result = &pb.RequestEndResult{
		Result: &pb.RequestEndResult_Void{
			Void: &pb.HttpVoidResponse{
				Id: response.ID[:],
			},
		},
	}
	r.Send(result)

	return nil
}

func (s *PluginServiceGrpc) execRequestEnd(response *Response) (*Response, error) {
	s.muE.Lock()
	defer s.muE.Unlock()
	return s.Plugin.RequestEnd(response)
}

func pollCache(c pb.Plugin_CacheEndServer, into map[uuid.UUID]*Cache) (*bool, *Cache, error) {
	cacheProto, err := c.Recv()

	if err == io.EOF {
		for _, cache := range into {
			cache.Bytes <- nil
		}
		return nil, nil, nil
	}

	if err != nil {
		for _, cache := range into {
			cache.Err <- err
		}
		return nil, nil, err
	}

	id, err := uuid.FromBytes(cacheProto.Id)
	if err != nil {
		return nil, nil, err
	}

	added := false
	if into[id] == nil {
		contentChan := make(chan []byte, 1)
		errChan := make(chan error)

		into[id], _ = CacheResFromProto(cacheProto, contentChan, errChan)
		added = true
	}

	if len(cacheProto.ResponseByte) == 0 {
		into[id].Bytes <- nil
	} else {
		into[id].Bytes <- cacheProto.ResponseByte
	}

	return &added, into[id], nil
}

func (s *PluginServiceGrpc) CacheStart(c pb.Plugin_CacheStartServer) error {
	log.Println("CacheStart")
	for {
		log.Println("CacheStart Loop")
		cacheProto, err := c.Recv()
		log.Println("CacheStart Recv")

		if err == io.EOF {
			log.Println("CacheStart EOF")
			return nil
		}

		if err != nil {
			return err
		}

		cacheRequest, err := CacheReqFromProto(cacheProto)
		if err != nil {
			return err
		}

		res, err := s.Plugin.CacheStart(cacheRequest)
		if err != nil {
			return err
		}

		if res == nil {
			result := &pb.CacheStartResult{
				Result: &pb.CacheStartResult_Void{
					Void: &pb.CacheVoidResponse{
						Id: cacheRequest.ID[:],
					},
				},
			}
			c.Send(result)
		} else {
		outer:
			for {
				select {
				case chunk := <-res.Bytes:
					if chunk == nil {
						c.Send(&pb.CacheStartResult{
							Result: &pb.CacheStartResult_Response{
								Response: res.ToProto(make([]byte, 0)),
							},
						})

						log.Println("CacheStart final bytes sent")
						break outer
					}
					result := &pb.CacheStartResult{
						Result: &pb.CacheStartResult_Response{
							Response: res.ToProto(chunk),
						},
					}
					c.Send(result)
					log.Println("CacheStart bytes sent")
				case err := <-res.Err:
					log.Println("CacheStart Error", err)
					return err
				}
			}
		}

	}
}

func (s *PluginServiceGrpc) CacheEnd(c pb.Plugin_CacheEndServer) error {
	caches := make(map[uuid.UUID]*Cache)
	cachesChan := make(chan *Cache)
	errChan := make(chan error)
	quitChan := make(chan int)
	wg := sync.WaitGroup{}

	go func() {
		for {
			added, cache, err := pollCache(c, caches)

			if added != nil && *added {
				cachesChan <- cache
				continue
			}

			if err != nil {
				errChan <- err
				return
			}

			if cache == nil {
				quitChan <- 0
				return
			}
		}
	}()

	for {
		select {
		case cache := <-cachesChan:
			go func() {
				wg.Add(1)
				defer wg.Done()

				err := s.Plugin.CacheEnd(cache)
				if err != nil {
					errChan <- err
					return
				}
				delete(caches, cache.ID)
				c.Send(&pb.CacheVoidResponse{
					Id: cache.ID[:],
				})
			}()
		case err := <-errChan:
			return err
		case <-quitChan:
			wg.Wait()
			return nil
		}
	}
}
