package carpdriver_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/carp-suite/plugins/carpdriver"
)

func TestCookiesStringToArray(t *testing.T) {
	a := []*int{new(int), new(int), new(int)}
	b := []*int{new(int), new(int), new(int)}

	assert.Equal(t, a, b)

	cookieString := "PHPSESSID=298zf09hf012fh2; Domain=foo.example.com; Path=/; Expires=2015-10-21T07:28:00Z, csrftoken=u32t4o3tb3gg43; Domain=foo.example.com; Path=/; Expires=2015-10-21T07:30:00Z, _gat=1; Domain=foo.example.com; Path=/; Expires=2015-10-21T07:28:00Z; Secure"

	expected := make([]*carpdriver.Cookie, 0)

	expected = append(expected, &carpdriver.Cookie{
		Domain:  "foo.example.com",
		Path:    "/",
		Name:    "PHPSESSID",
		Value:   "298zf09hf012fh2",
		Expires: time.Date(2015, 10, 21, 07, 28, 00, 00, time.UTC),
	})

	expected = append(expected, &carpdriver.Cookie{
		Domain:  "foo.example.com",
		Path:    "/",
		Name:    "csrftoken",
		Value:   "u32t4o3tb3gg43",
		Expires: time.Date(2015, 10, 21, 07, 30, 00, 00, time.UTC),
	})

	expected = append(expected, &carpdriver.Cookie{
		Domain:  "foo.example.com",
		Path:    "/",
		Name:    "_gat",
		Value:   "1",
		Expires: time.Date(2015, 10, 21, 07, 28, 00, 00, time.UTC),
		Secure:  true,
	})

	obtained := carpdriver.CookiesStringToArray(cookieString)

	for i, o := range obtained {
		assert.Equal(t, o, expected[i], "Expected %v, but there is %v", o, obtained[i])
	}
}

func TestCookieStringToArray(t *testing.T) {
	cookieString := "PHPSESSID=298zf09hf012fh2; Domain=foo.example.com; Path=/; Expires=2015-10-21T07:28:00Z"

	expected := make([]*carpdriver.Cookie, 0)

	expected = append(expected, &carpdriver.Cookie{
		Domain:  "foo.example.com",
		Path:    "/",
		Name:    "PHPSESSID",
		Value:   "298zf09hf012fh2",
		Expires: time.Date(2015, 10, 21, 07, 28, 00, 00, time.UTC),
	})

	obtained := carpdriver.CookiesStringToArray(cookieString)

	assert.Equal(t, obtained, expected, "Expected %v, but there is %v", expected, &obtained)
}

func TestCookiesToHeaderString(t *testing.T) {
	cookies := make([]*carpdriver.Cookie, 0)
	cookies = append(cookies, &carpdriver.Cookie{
		Domain:  "foo.example.com",
		Path:    "/",
		Name:    "PHPSESSID",
		Value:   "298zf09hf012fh2",
		Secure:  true,
		Expires: time.Date(2015, 10, 21, 07, 28, 00, 0, time.UTC),
	})
	cookies = append(cookies, &carpdriver.Cookie{
		Domain:  "foo.example.com",
		Path:    "/",
		Name:    "csrftoken",
		Value:   "u32t4o3tb3gg43",
		Secure:  true,
		Expires: time.Date(2015, 10, 21, 07, 30, 00, 0, time.UTC),
	})
	cookies = append(cookies, &carpdriver.Cookie{
		Domain:  "foo.example.com",
		Path:    "/",
		Name:    "_gat",
		Value:   "1",
		Secure:  true,
		Expires: time.Date(2015, 10, 21, 07, 28, 00, 0, time.UTC),
	})

	expected := `PHPSESSID=298zf09hf012fh2; Secure; Domain=foo.example.com; Path=/; Expires=2015-10-21T07:28:00Z, csrftoken=u32t4o3tb3gg43; Secure; Domain=foo.example.com; Path=/; Expires=2015-10-21T07:30:00Z, _gat=1; Secure; Domain=foo.example.com; Path=/; Expires=2015-10-21T07:28:00Z`
	obtained := carpdriver.CookiesToHeaderString(cookies)
	assert.Equal(t, expected, obtained, "Expected %v, but there is %v", expected, obtained)
}

func TestCookieToHeaderString(t *testing.T) {
	cookies := make([]*carpdriver.Cookie, 0)
	cookies = append(cookies, &carpdriver.Cookie{
		Domain:  "foo.example.com",
		Path:    "/",
		Name:    "PHPSESSID",
		Value:   "298zf09hf012fh2",
		Secure:  true,
		Expires: time.Date(2015, 10, 21, 07, 28, 00, 0, time.UTC),
	})

	expected := "PHPSESSID=298zf09hf012fh2; Secure; Domain=foo.example.com; Path=/; Expires=2015-10-21T07:28:00Z"
	obtained := carpdriver.CookiesToHeaderString(cookies)
	assert.Equal(t, expected, obtained, "Expected %v, but there is %v", expected, obtained)
}
