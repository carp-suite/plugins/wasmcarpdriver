package carpdriver

import (
	"github.com/google/uuid"
	pb "gitlab.com/carp-suite/plugins/carpdriver/proto"
)

type Cache struct {
	ID    uuid.UUID
	Name  string
	Size  float64
	Err   chan error
	Bytes chan []byte
}

func CacheReqFromProto(c *pb.CacheRequest) (*Cache, error) {
	id, err := uuid.FromBytes(c.Id)
	if err != nil {
		return nil, err
	}

	cache := &Cache{
		ID:   id,
		Name: c.CacheName,
	}

	return cache, nil
}

func CacheResFromProto(c *pb.CacheResponse, bytesChan chan []byte, errChan chan error) (*Cache, error) {
	id, err := uuid.FromBytes(c.Id)
	if err != nil {
		return nil, err
	}

	cache := &Cache{
		ID:    id,
		Bytes: bytesChan,
		Err:   errChan,
		Size:  c.ResponseSize,
	}

	return cache, nil
}

func (c *Cache) ToProto(chunk []byte) *pb.CacheResponse {
	return &pb.CacheResponse{
		Id:           c.ID[:],
		ResponseSize: c.Size,
		ResponseByte: chunk,
	}
}
