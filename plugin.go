package carpdriver

type Plugin interface {
	Configure(ctx []byte) error
	RequestStart(req *Request) (*Request, *Response, error)
	RequestEnd(res *Response) (*Response, error)
	CacheStart(c *Cache) (*Cache, error)
	CacheEnd(c *Cache) error
}
