package carpdriver

import (
	"strings"

	fbs "github.com/google/flatbuffers/go"
	"github.com/google/uuid"
	"gitlab.com/carp-suite/plugins/carpdriver/flatbuffers"
	pb "gitlab.com/carp-suite/plugins/carpdriver/proto"
)

type Request struct {
	ID      uuid.UUID
	Method  Method
	Path    string
	Headers map[string]string
	Cookies []*Cookie
	Body    []byte
}

func ReqFromBytes(payload []byte) (*Request, error) {
	request := flatbuffers.GetRootAsRequest(payload, 0)

	id, err := uuid.FromBytes(request.IdBytes())
	if err != nil {
		return nil, err
	}

	req := &Request{
		ID:      id,
		Path:    string(request.Path()),
		Method:  Method(int(request.Method())),
		Headers: make(map[string]string),
		Cookies: make([]*Cookie, 0),
		Body:    request.BodyBytes(),
	}

	h := new(flatbuffers.Header)
	for i := 0; i < request.HeadersLength()/2; i++ {
		exists := request.Headers(h, i)

		if !exists {
			continue
		}

		headerKey := string(h.Key())
		headerValue := string(h.Value())

		if strings.ToLower(headerKey) == "cookie" {
			req.Cookies = CookiesStringToArray(headerValue)
			continue
		}

		req.Headers[headerKey] = headerValue
	}

	return req, nil
}

func (req *Request) ToBytes() ([]byte, error) {
	reqBuilder := new(fbs.Builder)
	id := reqBuilder.CreateByteVector(req.ID[:])
	path := reqBuilder.CreateByteString([]byte(req.Path))
	body := reqBuilder.CreateByteVector(req.Body)
	headers := make([]fbs.UOffsetT, len(req.Headers))

	for k, v := range req.Headers {
		key := reqBuilder.CreateByteString([]byte(k))
		value := reqBuilder.CreateByteString([]byte(v))
		flatbuffers.HeaderStart(reqBuilder)
		flatbuffers.HeaderAddKey(reqBuilder, key)
		flatbuffers.HeaderAddValue(reqBuilder, value)
		headers = append(headers, flatbuffers.HeaderEnd(reqBuilder))
	}

	if len(req.Cookies) != 0 {
		cookies := CookiesToHeaderString(req.Cookies)
		key := reqBuilder.CreateByteString([]byte("Cookie"))
		value := reqBuilder.CreateByteString([]byte(cookies))
		flatbuffers.HeaderStart(reqBuilder)
		flatbuffers.HeaderAddKey(reqBuilder, key)
		flatbuffers.HeaderAddValue(reqBuilder, value)
		headers = append(headers, flatbuffers.HeaderEnd(reqBuilder))
	}

	flatbuffers.RequestStartHeadersVector(reqBuilder, len(headers))
	for _, header := range headers {
		reqBuilder.PrependUOffsetT(header)
	}
	finalHeaders := reqBuilder.EndVector(len(headers))

	flatbuffers.RequestStart(reqBuilder)
	flatbuffers.RequestAddId(reqBuilder, id)
	flatbuffers.RequestAddPath(reqBuilder, path)
	flatbuffers.RequestAddMethod(reqBuilder, int16(req.Method))
	flatbuffers.RequestAddHeaders(reqBuilder, finalHeaders)
	flatbuffers.RequestAddBody(reqBuilder, body)
	reqRoot := flatbuffers.RequestEnd(reqBuilder)
	reqBuilder.Finish(reqRoot)
	return reqBuilder.FinishedBytes(), nil
}

func ReqFromProto(req *pb.HttpRequest) (*Request, error) {
	id, err := uuid.FromBytes(req.Id)
	if err != nil {
		return nil, err
	}

	request := &Request{
		ID:      id,
		Path:    req.Path,
		Method:  1,
		Headers: make(map[string]string),
		Cookies: make([]*Cookie, 0),
		Body:    req.Body,
	}

	if req.GetMethod() > 1 {
		request.Method = Method(req.Method)
	}

	for k, v := range req.Headers {
		if strings.ToLower(k) == "cookie" {
			request.Cookies = CookiesStringToArray(v)
			continue
		}
		request.Headers[k] = v
	}

	return request, nil
}

func (req *Request) ToProto() *pb.HttpRequest {
	result := &pb.HttpRequest{
		Id:      req.ID[:],
		Headers: req.Headers,
		Method:  pb.Method(int32(req.Method)),
		Body:    req.Body,
	}

	if len(req.Cookies) != 0 {
		result.Headers["Cookie"] = CookiesToHeaderString(req.Cookies)
	}

	return result
}
